Protheus
========
Protheus is a Open Source Cross-Platform Game Engine which is currently _under development_.
One of the goals of Protheus is to use minimal libraries and currently there's only GLFW, GLEW and SDL_net being used for Cross-Platform support.

There's a large list of future features and a few which to my knowledge haven't been seen before (hush hush), The entire engine is build around the idea of working effectively on multiple threads.

With that being said there are bugs and functionality might change at any time with no warning.
